<?php

namespace Jjanvier\BrokenOop;

/**
 * The "Tell Don't Ask" principle can be summarized as follows: the one which owns the data performs the action on it.
 * The client object "tells" what to do, instead of "asking" the data and performing an action with that data.
 *
 * Please note that the goal of the "Tell Don't Ask" principle is not to eradicate all getters or query functions
 * of an object.
 *
 * References:
 *  - Tell Don't Ask by Martin Fowler {@see https://martinfowler.com/bliki/TellDontAsk.html}
 *  - Getter Eradicators by Martin Fowler {@see https://martinfowler.com/bliki/GetterEradicator.html}
 *  - [French] Le langage objet passé, présent futur par Sophie Beaupuis {@see https://afup.org/talks/2215-le-langage-objet-passe-present-futur}
 *
 * @Annotation
 *
 * @author  Julien Janvier
 * @license GNU GPLv3 https://opensource.org/licenses/GPL-3.0
 */
class TellDontAsk
{

}
