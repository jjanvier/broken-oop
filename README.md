# Broken OOP

"Broken OOP" is a set of annotations to help describe common OOP problems met during code reads or code reviews. Each annotation explains simply and briefly an OOP design flaw. It contains small examples to ease understanding, as well as references to great authors that have already been thinking about that problem. 

## Motivation

Often, when I review code and I see a bunch of design flaws or glitches that could be fixed or improved, I face two problems:

1. Explaining clearly why this could be improved can be complex or time-consuming. It's a pitty because a lot of great minds already wrote about that particular flaw.

2. Sometimes, I have no time to refactor right now, but still, I don't want to lose the fact that this piece of code could be improved. Some other times, due to a lack of knowledge of the project, I don't know all the consequences of such a change, and I'd just like to highlight the problem to the main developer(s).

Thus, "Broken OOP" is an _educational tool_ which aims to create a shared understanding around OOP common design problems.

## Inspiration

Having an educational tool is great, but it's useless if it can't be used seamlessly by developers. The primary source of knowledge (both technical and functional) should be the code. Thanks to [Cyrille Martraire](https://twitter.com/cyriux) for his book [Living Documentation](https://leanpu.com/livingdocumentation) where he describes how powerful and simple annotations can be for developers.

PHP annotations don't exist out of the box, and the existing implementations are really far from what offers Java for instance; but still, it remains a good simple tool for such a project.

## Getting started

### Installation

Please, do not use this tool in production, but rather just for development.

> composer require --dev "jjanvier/broken-oop"

### How to use?

As this project uses [Doctrine annotations](https://github.com/doctrine/annotations), it is compatible with many IDE or text editors. Typically, it works with PHPStorm or Eclipse via the Symfony2 Plugin. To know more, please report to the [Doctrine annotations' documentation](https://www.doctrine-project.org/projects/doctrine-annotations/en/1.6/index.html#ide-support).

That means that by using _CTRL+click_ in your favorite IDE, you should be able to have a look at the annotation and to better understand the problem spotted by your peer.

todo gif with phpstorm

## Contribute

Here is the list of rules to follow to add a new annotation:
- Each annotation should spot a common OOP design flaw.
- Each annotation should be named after the problem it highlights or by a general principle whose name already emphasizes the problem. It should not be named after its solution.
- Each annotation must contain a description. The description should remain simple. Its role is just to introduce the problem, not to supplant great blog posts or books.
- Each annotation must come with several good English free references.
- Each annotation can come with good non free references, like a book for instance.
- Each annotation can come with several good non English free references.
- Each annotation must come with examples in the `examples` directory:
    - The folder must have the name of the annotation.
    - A file `broken.php` describes the problem.
    - A file `enhanced.php` describes a way to fix partially or completely this problem, and only this problem. Please note the file is not called `fixed.php` on purpose. Sometimes, we'll propose something better, but still not good. Sometimes we'll solve a part of a problem, but another one remains. 
    - Those files must be simple OOP examples.
    - The differences between those two files must be the smallest possible.

If needed, it's possible to use targets and attribute types thanks to the flexibility of [Doctrine's annotations](https://www.doctrine-project.org/projects/doctrine-annotations/en/latest/custom.html#custom-annotation-classes).

Here is a template of new annotation:

```php
<?php

namespace Jjanvier\BrokenOop;

/**
 * Here goes a simple description of this common OOP design flaw.
 *
 * References:
 *  - First English free reference {@see ...}
 *  - Second English free reference {@see ...}
 *  - First English non free reference {@see ...}
 *  - [French] First French free reference {@see ...}
 *
 * @Annotation
 *
 * @author  Foo Bar
 * @license GNU GPLv3 https://opensource.org/licenses/GPL-3.0
 */
class CommonOOPDesignFlaw
{

}
```

## License

[GNU General Public License version 3](https://opensource.org/licenses/GPL-3.0)
