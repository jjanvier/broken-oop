<?php

/**
 * @\Jjanvier\BrokenOop\TellDontAsk()
 */
class ForbiddenDirectory
{
    private $path;

    public function __construct(string $path)
    {
        if (!is_dir($path)) {
            throw new \Exception('The path does not exist.');
        }

        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }
}

$forbiddenDirectories = [
    new ForbiddenDirectory('/foo/bar'),
    new ForbiddenDirectory('/foo/baz'),
];

// ...

foreach ($forbiddenDirectories as $forbiddenDirectory) {
    // Here the "Tell Don't Ask" principle is broken.
    // We retrieve data ("ask") from an object whereas the object could perfectly perform the action by itself ("tell").
    rmdir($forbiddenDirectory->getPath());
}
