<?php

class ForbiddenDirectory
{
    private $path;

    public function __construct(string $path)
    {
        if (!is_dir($path)) {
            throw new \Exception('The path does not exist.');
        }

        $this->path = $path;
    }

    public function remove(): void
    {
        rmdir($this->path);
    }
}

$forbiddenDirectories = [
    new ForbiddenDirectory('/foo/bar'),
    new ForbiddenDirectory('/foo/baz'),
];

// ...

foreach ($forbiddenDirectories as $forbiddenDirectory) {
    $forbiddenDirectory->remove();
}
